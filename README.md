
pgn2js - converts chess .pgn file (one tournament)
 to short .js file that contains variable definition (kind of .json).

Helpers, create short codes:

    1) #c (csharp) code
    Slow. Utility processes a .pgn file and creates .js file


    2) Lua with LuaJIT code
    Faster. The same - processes a .pgn file and creates .js file

Sample is in toursample.htm
Online: https://chessforeva.codeberg.page/chess_3D_tourn_sample/toursample.htm

Use tool to 3D-view one game:

https://chessforeva.gitlab.io/C0_pgn_url.htm?g=[E_Carlsen%20Tour%20Final%202020][D_2020.08.09][R_1.11][W_Ding,%20Liren][B_Carlsen,%20Magnus][Z_1-0][w_2791][b_2863][C_B90]nfnfrbHBahykcodzglhraqIlEiFuIvDqyBqAvAAlsImtdpfgagrdreHtadapfjlnpBnnnmjqlaadaspjbfpotij

ToDo: Whole tournament parser
